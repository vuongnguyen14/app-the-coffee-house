package com.example.baithi;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewPaperAdapter_thanhvien extends FragmentPagerAdapter {
    private final List<Fragment> lstFragment254 =  new ArrayList<>();
    private final List<String> lstTitle254 = new ArrayList<>();

    public ViewPaperAdapter_thanhvien(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return lstFragment254.get(position);
    }

    @Override
    public int getCount() {
        return lstTitle254.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return lstTitle254.get(position);
    }

    public void AddFragment (Fragment fragment, String string){
        lstFragment254.add(fragment);
        lstTitle254.add(string);
    }
}
