package com.example.baithi;

public class Contact {
    private int mImage254;
    private String mName254;

    public Contact(int mImage1,String mName) {
        this.mImage254 = mImage1;
        this.mName254 = mName;

    }

    public int getmImage1() {
        return mImage254;
    }

    public void setmImage1(int mImage1) {
        this.mImage254 = mImage1;
    }

    public String getmName() {
        return mName254;
    }

    public void setmName(String mName) {
        this.mName254 = mName;
    }
}