package com.example.baithi;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import douong.DoUong;

public class RecycleViewAdapter1 extends RecyclerView.Adapter<RecycleViewAdapter1.MyViewHolder> {
    Context mContext254;
    List<DoAn> mData254;

    public RecycleViewAdapter1(Context mContext, List<DoAn>mData) {
        this.mContext254 = mContext;
        this.mData254 = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext254).inflate(R.layout.item_thucuong, parent, false);
        MyViewHolder vHolder = new MyViewHolder(v);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.textViewName.setText(mData254.get(position).getName());
        holder.textViewGia.setText(mData254.get(position).getGia());
        holder.imageViewDoUong.setImageResource(mData254.get(position).getPhoto());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext254, mData254.get(position).getName(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext254.getApplicationContext(), chitietsanpham.class);
                //    Intent intent = new Intent(mContext,chitietsanpham.class);
                intent.putExtra("name",mData254.get(position).getName());
                intent.putExtra("image",mData254.get(position).getPhoto());
                intent.putExtra("gia",mData254.get(position).getGia());
                mContext254.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return mData254.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView textViewName;
        TextView textViewGia;
        private ImageView imageViewDoUong;

        public MyViewHolder(View itemView){
            super(itemView);
            textViewName = (TextView) itemView.findViewById(R.id.tv_ten);
            textViewGia = (TextView) itemView.findViewById(R.id.tv_tien);
            imageViewDoUong = (ImageView) itemView.findViewById(R.id.img_thucuong);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    listener.onItemClick(new Imt());
//                }
//            });

        }
    }

}