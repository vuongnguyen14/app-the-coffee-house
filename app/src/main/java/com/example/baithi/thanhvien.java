package com.example.baithi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;


public class thanhvien extends AppCompatActivity {
    private TabLayout tablayout254;
    private ViewPager viewPaper254;
    private ViewPaperAdapter_thanhvien adapter254;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanhvien);

        tablayout254 = findViewById(R.id.tab_layout);
        viewPaper254 = findViewById(R.id.view_paper);
        adapter254 = new ViewPaperAdapter_thanhvien(getSupportFragmentManager());

        adapter254.AddFragment(new Moi(),"Mới");
        adapter254.AddFragment(new Dong(),"Đồng");
        adapter254.AddFragment(new Bac(),"Bạc");
        adapter254.AddFragment(new Vang(),"Vàng");
        adapter254.AddFragment(new KimCuong(),"Kim Cương");
        viewPaper254.setAdapter(adapter254);
        tablayout254.setupWithViewPager(viewPaper254);

        tablayout254.getTabAt(0).setIcon(R.drawable.ic_events);
        tablayout254.getTabAt(1).setIcon(R.drawable.ic_events1);
        tablayout254.getTabAt(2).setIcon(R.drawable.ic_events4);
        tablayout254.getTabAt(3).setIcon(R.drawable.ic_events3);
        tablayout254.getTabAt(4).setIcon(R.drawable.ic_events2);
        ImageView im_close_thanhvien = (ImageView)findViewById(R.id.close_thanhvien);
        im_close_thanhvien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(thanhvien.this, myaccount.class);
                startActivity(intent);
            }
        });
    }
}