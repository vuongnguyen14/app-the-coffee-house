package com.example.baithi;

public class DoAn {
    private String Name254;
    private String Gia254;
    private int Photo254;

    public DoAn(int photo, String name, String gia ) {
        Name254 = name;
        Gia254 = gia;
        Photo254 = photo;

    }

    public DoAn() {
    }
    public String getName() {
        return Name254;
    }

    public void setName(String name) {
        Name254 = name;
    }

    public String getGia() {
        return Gia254;
    }

    public void setGia(String gia) {
        Gia254 = gia;
    }

    public int getPhoto() {
        return Photo254;
    }

    public void setPhoto(int photo) {
        Photo254 = photo;
    }
}