package Login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.baithi.MainActivity;
import com.example.baithi.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
   Button bt_register254, btlogin254;
    TextView email254 , pass254;
    ProgressDialog pd254;
    private FirebaseAuth mAuth254;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth254 = FirebaseAuth.getInstance();
        email254 = findViewById(R.id.edtEmail);
        pass254 = findViewById(R.id.edtPassword);
        btlogin254=(Button)findViewById(R.id.login_btn);
        btlogin254.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String email = email254.getText().toString().trim();
                        String password  = pass254.getText().toString().trim();
                        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                            email254.setError("Email không hợp lệ!");
                            email254.setFocusable(true);
                        }else {
                            loginUser(email,password);

                        }
                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
        pd254 = new ProgressDialog(this);
        pd254.setMessage("Đang đăng nhập...");

        bt_register254=(Button)findViewById(R.id.register_btn);
        bt_register254.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
    private void loginUser(String email, String password) {
        pd254.show();
        mAuth254.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            pd254.dismiss();
                            FirebaseUser user = mAuth254.getCurrentUser();

                            startActivity(new Intent(LoginActivity.this,MainActivity.class));
                            finish();
                        } else {
                            pd254.dismiss();
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(LoginActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}

